#ifndef UNMOUNT_H_INCLUDED
#define UNMOUNT_H_INCLUDED
#include "utils.h"

struct unmount{
  char * id;
  bool valid;
}typedef unMount;

unMount * newUnMount(char * id, bool valid);

#endif // UNMOUNT_H_INCLUDED
