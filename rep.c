#include "rep.h"
Rep * newRep(char * name, char * path, char * id, bool valid){
  Rep * tmp = malloc(sizeof(Rep));

  tmp->name = newString(5);
  tmp->path = newString(PATH_LENGHT);
  tmp->id = newString(8);
  tmp->valid = valid;

  tmp->name = name;
  tmp->path = path;
  tmp->id = id;

  if(strlen(path)==0){
    tmp->valid = FALSE;
    alert(WAR, "parametro -path OBLIGATORIO");
  }

  if(strlen(name) == 0){
    tmp->valid = FALSE;
    alert(WAR, "parametro -name OBLIGATORIO");
  }else{
    if(stricmp(name, "mbr") != 0 && stricmp(name, "disk") != 0){
      tmp->valid = FALSE;
      alert(ALR, "valor -name NO RECONOCIDO");
      tmp->name = newString(16);
    }
  }

  if(strlen(id) == 0){
    tmp->valid = FALSE;
    alert(WAR, "parametro -id OBLIGATORIO");
  }
  return tmp;
}
