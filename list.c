#include "list.h"
void insert_front(list* _list, char* id, char* path, char* name)
{
    disk* new_node = create_disk(id,path,name, _list->head);
    _list->head = new_node;
}

void insert_disk_top(list* _list, disk* _disk){
    disk* cursor = _list->head;
    disk* prev = NULL;
    disk* new_node = create_disk(_disk->id,_disk->path,_disk->name, NULL);

    if(cursor==NULL){
        _list->head = new_node;
    }else if(cursor->next==NULL){
        if(strcmp(new_node->name,cursor->name)>=0){
            new_node->next = cursor;
            cursor->next = NULL;
        }else{
            cursor->next = new_node;
        }
    }else{
        while(cursor->next!=NULL&&strcmp(new_node->name,cursor->name)>=0){
            prev = cursor;
            cursor = cursor->next;
        }
        if(prev!=NULL){
        prev->next = new_node;
        new_node->next = cursor;
        }

    }

}

void delete_front(list* _list)
{
    if(_list->head == NULL){

    }else{
        disk *front = _list->head;
        _list->head = _list->head->next;
        front->next = NULL;
        /* is this the last node in the list */
        if(front == _list->head)
            _list->head = NULL;
        free(front);
   }
}

//COLA
void insert_back(list* _list, char* id, char* path, char* name)
{
    if(_list->head == NULL){
        insert_front(_list,id,path,name);
    }else{
    /* go to the last node */
    disk *cursor = _list->head;
    while(cursor->next != NULL)
        cursor = cursor->next;

    /* create a new node */
    disk* new_node =  create_disk(id,path,name, NULL);
    cursor->next = new_node;
    }
}
void delete_back(list* _list){
    if(_list->head == NULL){
        _list->head = NULL;
    }else{
        disk *cursor = _list->head;
        disk *back = NULL;
        while(cursor->next != NULL)
        {
            back = cursor;
            cursor = cursor->next;
        }

        if(back != NULL)
            back->next = NULL;

        /* if this is the last node in the list*/
        if(cursor == _list->head)
            _list->head = NULL;

        free(cursor);
    }
}

void remove_any(list* _list, char* id){
    disk* nd = search(_list->head,_list->head,id);
    if(nd == NULL){
      alert(ERR, "La particion que desea desmontar no se encuentra montada actualmente.");
    }

    else if(nd == _list->head){
         delete_front(_list);
         alert(SCS, "Particion desmontada Exitosamente.");
    }
    /* if the node is the last node */
    else if(nd->next == NULL){
         delete_back(_list);
         alert(SCS, "Particion desmontada Exitosamente.");
    }
    /* if the node is in the middle */
    else{
        disk* cursor = _list->head;
        while(cursor != NULL)
        {
            if(cursor->next == nd)
                break;
            cursor = cursor->next;
        }

        if(cursor != NULL)
        {
            disk* tmp = cursor->next;
            cursor->next = tmp->next;
            tmp->next = NULL;
            free(tmp);
        }
        alert(SCS, "Particion desmontada Exitosamente.");
    }
}

disk* search(disk* head, disk* cursor, char* id){//SEARCH FOR A VALUE (IT'S USED BY REMOVE FUNCTION
if(cursor!=NULL){
    if(cursor->next!=head)
    {
        if(strcmp(cursor->id,id)==0){
            return cursor;
        }
        search(head,cursor->next,id);

    }else {
        if(strcmp(cursor->id,id)==0){
            return cursor;
        }else{
            return NULL;
        }
    }
}else{
    return NULL;
}
}



void dispose(list* _list){
    disk *cursor, *tmp;

    if(_list->head != NULL)
    {
        cursor = _list->head;
        _list->head = NULL;
        while(cursor != NULL)
        {
            tmp = cursor->next;
            free(cursor);
            cursor = tmp;
        }
    }
}
void print(list* _list){
     disk* cursor = _list->head;
    if(_list->head != NULL){
        while(cursor != NULL)
        {
            printf(cursor->id);
            //printf("\n");
            //printf(cursor->path);
            //printf("\n");
            //printf(cursor->name);
             //printf("\n");
            if(cursor->next!=NULL){
            printf(" -> ");
            }
            cursor = cursor->next;
        }
    }
     printf("\n");
}
