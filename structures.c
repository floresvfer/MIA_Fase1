#include "structures.h"

const char PARTITION_STATUS_NOT_ACTIVE = 'F';
const char PARTITION_STATUS_ACTIVE = 'T';
const char PARTITION_TYPE_PRIMARY = 'P';
const char PARTITION_TYPE_EXTENDED = 'E';

void configEmptyPartition(Partition * part){
  part->part_status = PARTITION_STATUS_NOT_ACTIVE;
  part->part_type = '\0';
  strncpy(part->part_fit, "\0\0",2);
  part->part_start = 0;
  part->part_size = 0;
  strncpy(part->part_name, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 15);
}

void configEmptyPartitionFast(Partition * part){
  part->part_status = PARTITION_STATUS_NOT_ACTIVE;
}

void configPartition(Partition * part, char status, char type, char * fit, int start, int size, char * name){
  part->part_status = status;
  part->part_type = type;
  strncpy(part->part_fit, fit,2);
  part->part_start = start;
  part->part_size = size;
  strncpy(part->part_name, name, 15);
}

void sortPartitions(Mbr * mbr){
  Partition *array[4];
  for(int i = 0; i<4; i++){
    array[i] = &(mbr->mbr_partition[i]);
  }

  for (int i = 0; i < 4; i++)
  {
    for (int j = 1; j < 4; j++)
    {
      if(array[j-1]->part_start > array[j]->part_start){

        Partition *tmp = (Partition*) malloc(sizeof(Partition));

        configPartition(tmp,
                        array[j-1]->part_status,
                        array[j-1]->part_type,
                        array[j-1]->part_fit,
                        array[j-1]->part_start,
                        array[j-1]->part_size,
                        array[j-1]->part_name);
        configPartition(&(mbr->mbr_partition[j-1]),
                        array[j]->part_status,
                        array[j]->part_type,
                        array[j]->part_fit,
                        array[j]->part_start,
                        array[j]->part_size,
                        array[j]->part_name);
        configPartition(&(mbr->mbr_partition[j]),
                        tmp->part_status,
                        tmp->part_type,
                        tmp->part_fit,
                        tmp->part_start,
                        tmp->part_size,
                        tmp->part_name);
        free(tmp);
      }
    }
  }
  compressPartitions(mbr);
}

void compressPartitions(Mbr * mbr){
  Partition *array[4];
  for(int i = 0; i<4; i++){
    array[i] = &(mbr->mbr_partition[i]);
  }

  for (int i = 0; i < 4; i++)
  {
    for (int j = 1; j < 4; j++)
    {
      if(array[j-1]->part_status < array[j]->part_status){

        Partition *tmp = (Partition*) malloc(sizeof(Partition));

        configPartition(tmp,
                        array[j-1]->part_status,
                        array[j-1]->part_type,
                        array[j-1]->part_fit,
                        array[j-1]->part_start,
                        array[j-1]->part_size,
                        array[j-1]->part_name);
        configPartition(&(mbr->mbr_partition[j-1]),
                        array[j]->part_status,
                        array[j]->part_type,
                        array[j]->part_fit,
                        array[j]->part_start,
                        array[j]->part_size,
                        array[j]->part_name);
        configPartition(&(mbr->mbr_partition[j]),
                        tmp->part_status,
                        tmp->part_type,
                        tmp->part_fit,
                        tmp->part_start,
                        tmp->part_size,
                        tmp->part_name);

        free(tmp);
      }
    }
  }
}


void getDate(char * destiny){
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    if(tm.tm_mday < 10 && (tm.tm_mon + 1) < 10){
        sprintf(destiny, "%c%d%c%d%d %d:%d", '0', tm.tm_mday, '0', tm.tm_mon + 1,tm.tm_year+1900,tm.tm_hour,tm.tm_sec);
    }else if(tm.tm_mday < 10 && tm.tm_mon > 10){
        sprintf(destiny, "%c%d%d%d %d:%d", '0', tm.tm_mday, tm.tm_mon +1,tm.tm_year+1900,tm.tm_hour,tm.tm_sec);
    }else if(tm.tm_mday > 10 && tm.tm_mon < 10){
        sprintf(destiny, "%d%c%d%d %d:%d", tm.tm_mday, '0', tm.tm_mon + 1,tm.tm_year+1900,tm.tm_hour,tm.tm_sec);
    }else{
        sprintf(destiny, "%d%d%d %d:%d", tm.tm_mday,tm.tm_mon + 1,tm.tm_year+1900,tm.tm_hour,tm.tm_sec);
    }
}
