#include "mount.h"

mount * newMount(char * path, char * name, bool valid){

    mount * tmp = malloc(sizeof(mount));

    //inicializacion de variables
    tmp->path = newString(PATH_LENGHT);
    tmp->name = newString(16);
    tmp->valid = valid;

    //asignacion de variables
    tmp->path = path;
    tmp->name = name;

    //validacion de parametros OBLIGATORIOS
    if(strlen(path) == 0){
      tmp->valid = FALSE;
      alert(WAR, "parametro -path OBLIGATORIO");
    }

    if(strlen(name) == 0){
      tmp->valid = FALSE;
      alert(WAR, "parametro -name OBLIGATORIO");
    }

    return tmp;
}
