#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>


#include "structures.h"

typedef int bool;
#define TRUE  1
#define FALSE 0


#define FACTOR 1024

#define PATH_LENGHT 70

#define RED   "\x1B[1;31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[1;36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

#define ERR 0
#define SCS 1
#define WAR 2
#define TRM 3
#define ALR 4

void alert(int type, char * message);

void clearStr(char * str, int len);
char * newString(int len);
int stricmp (const char *p1, const char *p2);

bool isNumber(char * number);

bool isParameter(char * source);

bool getMbr(Mbr* mbr, char * filename);




#endif // UTILS_H_INCLUDED
