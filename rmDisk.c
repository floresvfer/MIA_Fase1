#include "rmDisk.h"
rmDisk * newRmDisk(char * path, bool valid){

  rmDisk * tmp = malloc(sizeof(rmDisk));

  //inicializacion de variables
  tmp->path = newString(PATH_LENGHT);
  tmp->valid = valid;

  //asignacion de variables
  tmp->path = path;

  //validacion de parametros
  if(strlen(path) == 0){
    tmp->valid = FALSE;
    alert(WAR, "parametro -path OBLIGATORIO");
  }

  return tmp;

}
