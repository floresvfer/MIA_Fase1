#include "commands.h"


int getCommand(char * command){

  char * tmp = newString(10);
  int index = 0;
  while (command[index]!=' ') {
    tmp[index] = command[index];
    index++;
  }
  index++;

  switch (commandType(tmp)) {
    case 0:
      alert(TRM, "mkdisk");
      command_mkdisk(command, index);
      break;

    case 1:
      alert(TRM, "rmdisk");
      command_rmdisk(command, index);
      break;

    case 2:
      alert(TRM, "fdisk");
      command_fdisk(command, index);
      break;

    case 3:
      alert(TRM, "mount");
      command_mount(command, index);
      break;

    case 4:
      alert(TRM, "unmount");
      command_unmount(command, index);
      break;

    case 5:
      alert(TRM, "exec");
      command_exec(command, index);
      break;

    case 6:
      alert(TRM, "rep");
      command_rep(command, index);
      break;

    default:
      alert(ERR, "comando no reconocido");
      break;
  }
  free(tmp);
}

int commandType(char * name){
  if(stricmp(name, "mkdisk") == 0)
    return 0;

  if(stricmp(name, "rmdisk") == 0)
    return 1;

  if(stricmp(name, "fdisk") == 0)
    return 2;

  if(stricmp(name, "mount") == 0)
    return 3;

  if(stricmp(name, "unmount") == 0)
    return 4;

  if(stricmp(name, "exec") == 0)
    return 5;

  if(stricmp(name, "rep") == 0)
    return 6;
}


void command_mkdisk(char * command, int index){

  char * path = "";
  char * unit = "";
  int size = -1;

  bool valid = TRUE;
  while(index < strlen(command)){
    if(command[index] == '-'){
      index++;
    }else{
      alert(ERR, "se esperaba '-' ");
      valid = FALSE;
      break;
    }

    char * type = newString(8);
    char * arg = newString(100);
    int index_type = 0;
    int index_arg = 0;

    while(command[index]!= '-'){
      type[index_type] = command[index];
      index++;
      index_type++;
    }
    index++;
    if(command[index]=='>'){
      index++;
      if(command[index]=='"'){
        index++;
        while (command[index] != '"') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
        index++;
      }else{
        while (command[index] != ' ') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
      }
      index++;

      if(stricmp(type, "path") == 0 ||
         stricmp(type, "unit") == 0 ||
         stricmp(type, "size") == 0){

           if(stricmp(type, "path") == 0){
             path = arg;
           }

           if(stricmp(type, "unit") == 0){
             if(strlen(arg) == 1){
                unit = arg[0];
             }else{
               alert(ERR, "Argumento no valido se esperaba UN CARACTER");
             }

           }

           if(stricmp(type, "size") == 0){
             if(isNumber(arg)){
               size = atoi(arg);
             }else{
               alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
             }
           }

      }else{
        alert(ERR, "no se reconoce el parametro especificado");
        valid = FALSE;
      }
    }else{
      alert(ERR, "se esperaba '>' ");
      valid = FALSE;
      break;
    }

  }

  newDisk(newMkDisk(path, unit, size, valid));

}


void command_rmdisk(char * command, int index){
  char * path = "";

  bool valid = TRUE;
  while(index < strlen(command)){
    if(command[index] == '-'){
      index++;
    }else{
      alert(ERR, "se esperaba '-' ");
      valid = FALSE;
      break;
    }

    char * type = newString(8);
    char * arg = newString(100);
    int index_type = 0;
    int index_arg = 0;

    while(command[index]!= '-'){
      type[index_type] = command[index];
      index++;
      index_type++;
    }
    index++;
    if(command[index]=='>'){
      index++;
      if(command[index]=='"'){
        index++;
        while (command[index] != '"') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
        index++;
      }else{
        while (command[index] != ' ') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
      }
      index++;

      if(stricmp(type, "path") == 0){
           if(stricmp(type, "path") == 0){
             path = arg;
           }
      }else{
        alert(ERR, "no se reconoce el parametro especificado");
        valid = FALSE;
      }
    }else{
      alert(ERR, "se esperaba '>' ");
      valid = FALSE;
      break;
    }

  }

  delDisk(newRmDisk(path, valid));

}


void command_fdisk(char * command, int index){
  int size = -1;
  char unit = ' ';
  char * path = "";
  char typep = ' ';
  char * fit = "";
  char * del = "";
  char * name = "";
  int add = 0;
  bool valid = TRUE;

  while(index < strlen(command)){
    if(command[index] == '-'){
      index++;
    }else{
      alert(ERR, "se esperaba '-' ");
      valid = FALSE;
      break;
    }

    char * type = newString(8);
    char * arg = newString(100);
    int index_type = 0;
    int index_arg = 0;

    while(command[index]!= '-'){
      type[index_type] = command[index];
      index++;
      index_type++;
    }
    index++;
    if(command[index]=='>'){
      index++;
      if(command[index]=='"'){
        index++;
        while (command[index] != '"') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
        index++;
      }else{
        while (command[index] != ' ') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
      }
      index++;

      if(stricmp(type, "path") == 0 ||
         stricmp(type, "unit") == 0 ||
         stricmp(type, "size") == 0 ||
         stricmp(type, "type") == 0 ||
         stricmp(type, "fit") == 0 ||
         stricmp(type, "delete") == 0 ||
         stricmp(type, "name") == 0 ||
         stricmp(type, "add") == 0)
         {

           if(stricmp(type, "path") == 0){
             path = arg;
           }

           if(stricmp(type, "unit") == 0){
             if(strlen(arg) == 1){
                unit = arg[0];
             }else{
               alert(ERR, "Argumento no valido se esperaba UN CARACTER");
             }
           }

           if(stricmp(type, "size") == 0){
             if(isNumber(arg)){
               size = atoi(arg);
             }else{
               alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
             }
           }

           if(stricmp(type, "type") == 0){
             if(strlen(arg) == 1){
                typep = arg[0];
             }else{
               alert(ERR, "Argumento no valido se esperaba UN CARACTER");
             }
           }

           if(stricmp(type, "fit") == 0){
             fit = arg;
           }

           if(stricmp(type, "delete") == 0){
             del = arg;
           }

           if(stricmp(type, "name") == 0){
             name = arg;
           }

           if(stricmp(type, "add") == 0){
             if(isNumber(arg)){
               add = atoi(arg);
             }else{
               alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
             }
           }

      }else{
        alert(ERR, "no se reconoce el parametro especificado");
        valid = FALSE;
      }
    }else{
      alert(ERR, "se esperaba '>' ");
      valid = FALSE;
      break;
    }

  }
  newPartition(newFDisk(size, unit, path, typep, fit, del, name, add, valid));
}


void command_mount(char * command, int index){
  char * path = "";
  char * name = "";

  bool valid = TRUE;
  while(index < strlen(command)){
    if(command[index] == '-'){
      index++;
    }else{
      alert(ERR, "se esperaba '-' ");
      valid = FALSE;
      break;
    }

    char * type = newString(8);
    char * arg = newString(100);
    int index_type = 0;
    int index_arg = 0;

    while(command[index]!= '-'){
      type[index_type] = command[index];
      index++;
      index_type++;
    }
    index++;
    if(command[index]=='>'){
      index++;
      if(command[index]=='"'){
        index++;
        while (command[index] != '"') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
        index++;
      }else{
        while (command[index] != ' ') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
      }
      index++;

      if(stricmp(type, "path") == 0 ||
         stricmp(type, "name") == 0 ){

           if(stricmp(type, "path") == 0){
             path = arg;
           }

           if(stricmp(type, "name") == 0){
            name = arg;
           }
      }else{
        alert(ERR, "no se reconoce el parametro especificado");
        valid = FALSE;
      }
    }else{
      alert(ERR, "se esperaba '>' ");
      valid = FALSE;
      break;
    }

  }

  mountPartition(newMount(path, name, valid));

}


void command_unmount(char * command, int index){
  char * id = "";

  bool valid = TRUE;
  while(index < strlen(command)){
    if(command[index] == '-'){
      index++;
    }else{
      alert(ERR, "se esperaba '-' ");
      valid = FALSE;
      break;
    }

    char * type = newString(8);
    char * arg = newString(100);
    int index_type = 0;
    int index_arg = 0;

    while(command[index]!= '-'){
      type[index_type] = command[index];
      index++;
      index_type++;
    }
    index++;
    if(command[index]=='>'){
      index++;
      if(command[index]=='"'){
        index++;
        while (command[index] != '"') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
        index++;
      }else{
        while (command[index] != ' ') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
      }
      index++;

      if(stricmp(type, "id") == 0 ){

           if(stricmp(type, "id") == 0){
             id = arg;
           }

      }else{
        alert(ERR, "no se reconoce el parametro especificado");
        valid = FALSE;
      }
    }else{
      alert(ERR, "se esperaba '>' ");
      valid = FALSE;
      break;
    }

  }

  unMountPartition(newUnMount(id, valid));
}


void command_exec(char * command, int index){
  printf("%s\n", "exec");

}


void command_rep(char * command, int index){
  char * id = "";
  char * path = "";
  char * name = "";
  bool valid = TRUE;

  while(index < strlen(command)){
    if(command[index] == '-'){
      index++;
    }else{
      alert(ERR, "se esperaba '-' ");
      valid = FALSE;
      break;
    }

    char * type = newString(8);
    char * arg = newString(100);
    int index_type = 0;
    int index_arg = 0;

    while(command[index]!= '-'){
      type[index_type] = command[index];
      index++;
      index_type++;
    }
    index++;
    if(command[index]=='>'){
      index++;
      if(command[index]=='"'){
        index++;
        while (command[index] != '"') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
        index++;
      }else{
        while (command[index] != ' ') {
          arg[index_arg] = command[index];
          index++;
          index_arg++;
        }
      }
      index++;

      if(stricmp(type, "path") == 0 ||
         stricmp(type, "name") == 0 ||
         stricmp(type, "id") == 0)
         {

           if(stricmp(type, "path") == 0){
             path = arg;
           }

           if(stricmp(type, "name") == 0){
             name = arg;
           }

           if(stricmp(type, "id") == 0){
             id = arg;
           }

      }else{
        alert(ERR, "no se reconoce el parametro especificado");
        valid = FALSE;
      }
    }else{
      alert(ERR, "se esperaba '>' ");
      valid = FALSE;
      break;
    }

  }
  genRep(newRep(name, path, id, valid));
}
