#include "mkDisk.h"
mkDisk * newMkDisk(char * path, char * unit, int size, bool valid){

    mkDisk * tmp = malloc(sizeof(mkDisk));

    //inicializacion de variables
    tmp->path = newString(PATH_LENGHT);
    tmp->unit = newString(5);
    tmp->size = -1;
    tmp->valid = valid;

    //asignacion de variables
    tmp->path = path;
    tmp->unit = unit;
    tmp->size = size;

    //validacion de parametros OBLIGATORIOS
    if(strlen(path) == 0){
      tmp->valid = FALSE;
      alert(WAR, "parametro -path OBLIGATORIO");
    }

    if(size == -1){
      tmp->valid = FALSE;
      alert(WAR, "parametro -size OBLIGATORIO");
    }

    //validacion de parametros VALORES


    if(unit != ' '){
      if(tmp->unit != 'k' && tmp->unit != 'm'){
        tmp->valid = FALSE;
        alert(ALR, "valor -unit NO RECONOCIDO");
        tmp->unit = ' ';
      }
    }else{
      tmp->unit = 'm';
    }

    return tmp;
}
