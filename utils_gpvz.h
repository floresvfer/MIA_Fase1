#ifndef UTILS_GPVZ_H_INCLUDED
#define UTILS_GPVZ_H_INCLUDED
#include "structures.h"
#include "utils.h"

bool graphMbr(Mbr * mbr, char * path);
bool graphDisk(Mbr * mbr, char * path);
double getScale(int sizeDisk, int sizePart);

#endif // UTILS_GPVZ_H_INCLUDED
