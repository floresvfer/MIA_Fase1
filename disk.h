#ifndef DISK_H_INCLUDED
#define DISK_H_INCLUDED
#include "structures.h"
#include "utils.h"
typedef struct disk{
  char * path;
  char * name;
  char * id;
  struct disk * next;
}disk;

disk * create_disk(char * id, char * path, char * name, disk * next);

#endif // DISK_H_INCLUDED
