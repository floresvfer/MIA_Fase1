#include "utils.h"

void alert(int type, char * message){
  switch (type) {
    case 0:
      printf(RED "ERROR: " RESET);
      printf(WHT "%s\n\n" RESET, message);
      break;

    case 1:
      printf(GRN "SUCCESS: " RESET);
      printf(WHT "%s\n\n" RESET, message);
      break;

    case 2:
      printf(YEL "WARNING: " RESET);
      printf(WHT "%s\n" RESET, message);
      break;

    case 3:
      printf(BLU "COMMAND: " RESET);
      printf(WHT "%s\n" RESET, message);
      break;

    case 4:
      printf(CYN "ALERT: " RESET);
      printf(WHT "%s\n" RESET, message);
      break;

    default:
      break;
  }

}

void clearStr(char * str, int len){
  int count = 0;
  while(count <= len){
    str[count] = '\0';
    count ++;
  }
}

char * newString(int len){
  char * tmp = (char *)malloc(sizeof(char)*len);
  clearStr(tmp, len);
  return tmp;
}

int stricmp (const char *p1, const char *p2)
{
  register unsigned char *s1 = (unsigned char *) p1;
  register unsigned char *s2 = (unsigned char *) p2;
  unsigned char c1, c2;

  do
  {
      c1 = (unsigned char) toupper((int)*s1++);
      c2 = (unsigned char) toupper((int)*s2++);
      if (c1 == '\0')
      {
            return c1 - c2;
      }
  }
  while (c1 == c2);

  return c1 - c2;
}

bool isNumber(char * number){
  int index = 0;
  while(index < strlen(number)){

    if( !((int)number[index] >= 48 && (int)number[index] <= 57) && ((int)number[index] != 45))
      return FALSE;

    index++;
  }
  return TRUE;
}

bool isParameter(char * source)
{
    return strstr(source, "->") != NULL;
}

bool getMbr(Mbr* mbr, char * filename)
{
    FILE *file;
    file = fopen(filename,"r+b");
    if (!file)
    {
        alert(ERR, "No se pudo obtener el MBR -");
        return FALSE;
    }
    fread(mbr,sizeof(Mbr),1,file);
    fclose(file);
    return TRUE;
}
