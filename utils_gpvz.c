#include "utils_gpvz.h"
#include <stdio.h>

//sudo apt install python-pydot python-pydot-ng graphviz

bool graphMbr(Mbr * mbr, char * path){
    Partition *array[4];
    for(int i = 0; i<4; i++){
      array[i] = &(mbr->mbr_partition[i]);
    }
    Partition *(*p)[] = &array;

    FILE *fp;
    fp=fopen(path, "wb");

    fprintf(fp, "%s \n\n", "digraph example {");
    fprintf(fp, "%s\n", "graph [bgcolor=\"#000000\"];");
    //fprintf(fp, "%s \n", "node [shape=plaintext fontname="FreeMono"]");
    fprintf(fp, "%s \n", "node [shape=plaintext fontname=\"Ubuntu Condensed\"]");
    fprintf(fp, "%s \n", "rankdir=TB");

    fprintf(fp, "\t %s \n", "B [");
    fprintf(fp, "\t\t%s \n", "label=<");

    fprintf(fp, "\t\t\t%s \n", "<TABLE border=\"0\" cellspacing=\"1\" cellpadding=\"8\">");

    fprintf(fp, "\t\t\t\t%s \n", "<TR PORT=\"header\">");
    fprintf(fp, "\t\t\t\t\t<TD BGCOLOR=\"#FB0093\"  width=\"400\" COLSPAN=\"2\"><font color=\"#FFFFFF\">MBR %s </font></TD> \n", "Disk1.dsk");
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#1FD55F\"><font color=\"#2C1438\">Nombre</font></TD><TD align=\"left\"  BGCOLOR=\"#1FD55F\"><font color=\"#2C1438\">Valor</font></TD>\n");
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">mbr_tamanio</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",mbr->mbr_tamanio);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">mbr_fecha_creacion</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%s</font></TD>\n",mbr->mbr_fecha_creacion);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">mbr_disk_signature</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",mbr->mbr_disk_signature);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

     for (int i = 0; i < 4; i++)
      {
          Partition * partition = (*p)[i];

          if (partition->part_status == PARTITION_STATUS_ACTIVE){
                fprintf(fp, "\t\t\t\t%s \n", "<TR>");
                fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_status_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%c</font></TD>\n",(i+1),partition->part_status);
                fprintf(fp, "\t\t\t\t%s \n", "</TR>");

                fprintf(fp, "\t\t\t\t%s \n", "<TR>");
                fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_type_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%c</font></TD>\n",(i+1),partition->part_type);
                fprintf(fp, "\t\t\t\t%s \n", "</TR>");

                fprintf(fp, "\t\t\t\t%s \n", "<TR>");
                fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_fit_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%c%c</font></TD>\n",(i+1),partition->part_fit[0],partition->part_fit[1]);
                fprintf(fp, "\t\t\t\t%s \n", "</TR>");

                fprintf(fp, "\t\t\t\t%s \n", "<TR>");
                fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_start_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%d</font></TD>\n",(i+1),partition->part_start);
                fprintf(fp, "\t\t\t\t%s \n", "</TR>");

                fprintf(fp, "\t\t\t\t%s \n", "<TR>");
                fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_size_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%d</font></TD>\n",(i+1),partition->part_size);
                fprintf(fp, "\t\t\t\t%s \n", "</TR>");

                fprintf(fp, "\t\t\t\t%s \n", "<TR>");
                fprintf(fp, "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_name_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%s</font></TD>\n",(i+1),&(partition->part_name));
                fprintf(fp, "\t\t\t\t%s \n", "</TR>");

          }

      }

    fprintf(fp, "\t\t\t%s \n", "</TABLE>");


    fprintf(fp, "\t\t%s \n",">");
    fprintf(fp, "\t%s \n","];");
    fprintf(fp, "%s \n","}");


    fclose(fp);

    system("dot -Tpng -o /home/fernando/Desktop/mbr.png /home/fernando/Desktop/reps/mbr_rep.gv");

}

bool graphDisk(Mbr * mbr, char * path){
  Partition *array[4];
  for(int i = 0; i<4; i++){
    array[i] = &(mbr->mbr_partition[i]);
  }
  Partition *(*p)[] = &array;

  FILE *fp;
  fp=fopen(path, "wb");

  fprintf(fp, "%s \n\n", "digraph example {");
  fprintf(fp, "%s\n", "graph [bgcolor=\"#000000\"];");
  //fprintf(fp, "%s \n", "node [shape=plaintext fontname="FreeMono"]");
  fprintf(fp, "%s \n", "node [shape=plaintext fontname=\"Ubuntu Condensed\"]");
  fprintf(fp, "%s \n", "rankdir=TB");

  fprintf(fp, "\t %s \n", "B [");
  fprintf(fp, "\t\t%s \n", "label=<");

  fprintf(fp, "\t\t\t%s \n", "<TABLE border=\"1\" cellspacing=\"0\">");

  fprintf(fp, "\t\t\t\t%s \n", "<TR>");

  fprintf(fp, "\t\t\t\t\t%s \n", "<TD align=\"center\" width=\"100\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#FB0093\"><font color=\"#ffffff\">MBR</font></TD>");
  int lastIndex = 0;
  for (int i = 0; i < 4; i++)
   {
       Partition * partition = (*p)[i];
       Partition * partitionAux;

       if (partition->part_status == PARTITION_STATUS_ACTIVE){
         if(i==0 && partition->part_start != 136){

           fprintf(fp, "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#14091A\"><font color=\"#1FD55F\">LIBRE %.2f%c</font></TD>\n", (int)getScale(mbr->mbr_tamanio, freeSpace(136, partition->part_start)), ((getScale(mbr->mbr_tamanio, freeSpace(136, partition->part_start))/20)),'%');
         }else if(i>0){
           partitionAux = (*p)[i-1];
           if((partitionAux->part_start + partitionAux->part_size) != partition->part_start){
             fprintf(fp, "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#14091A\"><font color=\"#1FD55F\">LIBRE %.2f%c</font></TD>\n", (int)getScale(mbr->mbr_tamanio, freeSpace((partitionAux->part_start + partitionAux->part_size), partition->part_start)), ((getScale(mbr->mbr_tamanio, freeSpace((partitionAux->part_start + partitionAux->part_size), partition->part_start))/20)),'%');
           }
         }


         if(partition->part_type == 'p'){
           fprintf(fp, "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#1FD55F\"><font color=\"#14091A\">PRIMARIA %.2f%c</font></TD>\n", (int)getScale(mbr->mbr_tamanio, partition->part_size), ((getScale(mbr->mbr_tamanio, partition->part_size)/20)),'%');
         }
         lastIndex = i;
       }

   }


     Partition * partitionAux = (*p)[lastIndex];
     if((partitionAux->part_start + partitionAux->part_size) != mbr->mbr_tamanio){
       fprintf(fp, "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#14091A\"><font color=\"#1FD55F\">LIBRE %.2f%c</font></TD>\n", (int)getScale(mbr->mbr_tamanio, freeSpace((partitionAux->part_start + partitionAux->part_size), mbr->mbr_tamanio)), ((getScale(mbr->mbr_tamanio, freeSpace((partitionAux->part_start + partitionAux->part_size),mbr->mbr_tamanio))/20)),'%');
     }


   fprintf(fp, "\t\t\t\t%s \n", "</TR>");

   fprintf(fp, "\t\t\t%s \n", "</TABLE>");


   fprintf(fp, "\t\t%s \n",">");
   fprintf(fp, "\t%s \n","];");
   fprintf(fp, "%s \n","}");

   fclose(fp);

   system("dot -Tpng -o /home/fernando/Desktop/disk.png /home/fernando/Desktop/reps/disk_rep.gv");

}

double getScale(int sizeDisk, int sizePart){
  return ((((double)sizePart/(double)sizeDisk) * 100) * 20);
}

int freeSpace(int a, int b){
  return b-a;
}
