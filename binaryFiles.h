#ifndef BINARYFILES_H_INCLUDED
#define BINARYFILES_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#include "utils.h"
#include "utils_gpvz.h"

#include "structures.h"
#include "mkDisk.h"
#include "rmDisk.h"
#include "fDisk.h"
#include "mount.h"
#include "unMount.h"
#include "rep.h"
#include "list.h"

list * mounts;


bool newPartition(fDisk * data);
int getEspacioLibre(int ps1, int psz1, int ps2);
void newDisk(mkDisk * data);
void delDisk(rmDisk * data);
void genRep(Rep * data);
void mountPartition(mount * data);
void unMountPartition(unMount * data);
void initializeList();


#endif // BINARYFILES_H_INCLUDED
