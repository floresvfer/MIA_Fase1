#ifndef COMMANDS_H_INCLUDED
#define COMMANDS_H_INCLUDED

#include "utils.h"

#include "mkDisk.h"
#include "rmDisk.h"
#include "fDisk.h"
#include "mount.h"
#include "unMount.h"
#include "list.h"
#include "binaryFiles.h"

int getCommand(char * command);
int commandType(char * name);

void command_mkdisk(char * command, int index);
void command_rmdisk(char * command, int index);
void command_fdisk(char * command, int index);
void command_mount(char * command, int index);
void command_unmount(char * command, int index);
void command_exec(char * command, int index);
void command_rep(char * command, int index);

#endif // COMMANDS_H_INCLUDED
