#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED

#include <time.h>

extern const char PARTITION_STATUS_NOT_ACTIVE;
extern const char PARTITION_STATUS_ACTIVE;
extern const char PARTITION_TYPE_PRIMARY;
extern const char PARTITION_TYPE_EXTENDED;


typedef struct partition{
    char part_status;
    char part_type;
    char part_fit [2];
    int part_start;
    int part_size;
    char part_name [16];
}Partition;

typedef struct mbr{
    int mbr_tamanio;
    char  mbr_fecha_creacion [16]; // dd/mm/yyyy hh:ss
    int mbr_disk_signature;
    Partition mbr_partition [4];
}Mbr;

void configEmptyPartition(Partition * part);

void configEmptyPartitionFast(Partition * part);

void configPartition(Partition * part, char status, char type, char * fit, int start, int size, char * name);

void sortPartitions(Mbr * mbr);

void getDate(char * destiny);



#endif // STRUCTURES_H_INCLUDED
