#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED
#include "disk.h"

typedef struct list{
    disk* head;
}list;

void insert_front(list* _list, char* id, char* path, char* name);
void insert_disk_top(list* _list, disk* _disk);
void delete_front(list* _list);

void insert_back(list* _list, char* id, char* path, char* name);
void delete_back(list* _list);

void remove_any(list* _list, char* id);

disk* search(disk* head, disk* cursor, char* id);

void print(list* _list);
void dispose(list* _list);


#endif // LIST_H_INCLUDED
