#include "binaryFiles.h"
extern list * mounts;

void initializeList(){
  mounts = malloc(sizeof(list));
  mounts->head = NULL;
}

bool newPartition(fDisk * data){
  if(data->valid == TRUE)
  {

    if(data->size != -1){
      if(strlen(data->del) == 0 && data->add == 0)
      {

        int bytes;
        if(data->unit == 'b'){
            bytes = data->size;
        }
        else if(data->unit == 'k'){
            bytes = 1024*data->size;
        }else if(data->unit == 'm'){
            bytes = 1024*1024*data->size;
        }

        Mbr mbr;
        if (getMbr(&mbr, data->path))
        {
          Partition *array[4];
          for(int i = 0; i<4; i++){
            array[i] = &(mbr.mbr_partition[i]);
          }
          Partition *(*p)[] = &array;

          printf(CYN "################### ACCESANDO AL DISCO ###################\n" RESET);
          printf("date of disk creation: %s\n",mbr.mbr_fecha_creacion);
          printf("amount of bytes: %d \n", mbr.mbr_tamanio);
          printf("amount of mbr's bytes: %d \n", sizeof(Mbr));
          printf("amount of bytes to asign: %lu \n\n\n", bytes);

          /*for (int i = 0; i < 4; i++)
          {
              Partition * partition = (*p)[i];
              printf("status: %c\n", partition->part_status);
              printf("type: %c\n", partition->part_type);
              printf("fit: %s\n", partition->part_fit);
              printf("start: %d\n", partition->part_start);
              printf("size: %d\n", partition->part_size);
              printf("name: %s\n", partition->part_name);
              printf("%s\n\n",".........................");
          }*/

          int used = 0;
          bool oneExtended = FALSE;

          for (int i = 0; i < 4; i++)
          {
              Partition * partition = (*p)[i];

              if (partition->part_status == PARTITION_STATUS_ACTIVE)
                  used++;

              if (
                  partition->part_status == PARTITION_STATUS_ACTIVE &&
                  partition->part_type == PARTITION_TYPE_EXTENDED
                  )
                  oneExtended = TRUE;
          }

          // limite de particiones
          if (
                data->type == 'e' ||
                data->type == 'p'
            )
          {
              if (used >= 4)
              {
                  alert(ERR, "Ya existen 4 particiones creadas en el Disco seleccionado");
                  return FALSE;
              }
          }

          // solo una particion extendida
          if (
              data->type == 'e' &&
              oneExtended
          )
          {
              alert(ERR, "Ya existe una particion extendida en el Disco seleccionado");
              return FALSE;
          }

          // extendida necesaria para logicas
          if (
              data->type == 'l' &&
              !oneExtended
          )
          {
              alert(ERR, "No existe una particion extendida en el Disco seleccionado");
              return FALSE;
          }

          // VALIDACION DE NOMBRES
          char nombre[16];
          strncpy(nombre, data->name, 15);

          for (int i = 0; i < 4; i++)
          {
              Partition * partition = (*p)[i];

              if (partition->part_status == PARTITION_STATUS_ACTIVE)
              {
                  if (strcmp(&(partition->part_name), data->name) == 0)
                  {
                      alert(ERR, "Ya existe una particion con el mismo nombre en el Disco seleccionado");
                      return FALSE;
                  }
              }
          }

          int no_particiones = 0;
          int p_start = 0;
          int p_size = bytes;
          int free_space = 0;
          bool procede = TRUE;

          for(int i = 0; i<4; i++){
            Partition * partition = (*p)[i];
            if(partition->part_status == PARTITION_STATUS_ACTIVE){
              no_particiones++;
            }
          }

          if (data->type == 'p'){
            if(no_particiones == 0){
              //printf("%s\n", "disco vacio");
              free_space = getEspacioLibre(0, sizeof(Mbr), mbr.mbr_tamanio);
              printf("%s%d\n", "espacio libre: ",free_space);
              if(p_size <= free_space){
                p_start = sizeof(Mbr);
                printf("\t%s %s\n", "Existe epacio suficiente en disco, se Procedera a crear la particion: ", data->name);
                configPartition(&(mbr.mbr_partition[0]), PARTITION_STATUS_ACTIVE,
                                                         data->type,
                                                         data->fit,
                                                         p_start,
                                                         p_size,
                                                         data->name);
              }else{
                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
              }


            }else if(no_particiones == 1){
              Partition * tmp = &(mbr.mbr_partition[0]);

              free_space = sizeof(Mbr) - tmp->part_start;
              if(p_size <= free_space){
                p_start = sizeof(Mbr);
              }else{
                free_space = mbr.mbr_tamanio - (tmp->part_start + tmp->part_size);
                if(p_size <= free_space){
                  p_start = (tmp->part_start + tmp->part_size);
                }else{
                  procede == FALSE;
                }
              }


              if(procede == TRUE){
                printf("%s%d\n", "espacio libre: ",free_space);
                printf("\t%s %s\n", "Existe epacio suficiente en disco, se Procedera a crear la particion: ", data->name);
                configPartition(&(mbr.mbr_partition[1]), PARTITION_STATUS_ACTIVE,
                                                         data->type,
                                                         data->fit,
                                                         p_start,
                                                         p_size,
                                                         data->name);
              }else{
                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
              }


            }else if(no_particiones == 2){
              Partition * tmp = &(mbr.mbr_partition[0]);
              Partition * tmp1 = &(mbr.mbr_partition[1]);

              free_space = sizeof(Mbr) - tmp->part_start;
              //printf("espacio libre mbr-primeraParticion: %d\n", free_space);
              if(p_size <= free_space){
                p_start = sizeof(Mbr);
              }else{
                free_space = getEspacioLibre(tmp->part_start, tmp->part_size, tmp1->part_start);
                //printf("espacio libre primeraParticion-segundaParticion: %d\n", free_space);
                if(p_size <= free_space){
                  p_start = (tmp->part_start + tmp->part_size);
                }else{
                  free_space = mbr.mbr_tamanio - (tmp1->part_start + tmp1->part_size);
                  //printf("espacio libre segundaParticion-finDisco: %d\n", free_space);
                  if(p_size <= free_space){
                    p_start = (tmp1->part_start + tmp1->part_size);
                  }else{
                    procede == FALSE;
                  }
                }
              }


              if(procede == TRUE){
                printf("%s%d\n", "espacio libre: ",free_space);
                printf("\t%s %s\n", "Existe epacio suficiente en disco, se Procedera a crear la particion: ", data->name);
                configPartition(&(mbr.mbr_partition[2]), PARTITION_STATUS_ACTIVE,
                                                         data->type,
                                                         data->fit,
                                                         p_start,
                                                         p_size,
                                                         data->name);
              }else{
                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
              }

            }else if(no_particiones == 3){

              Partition * tmp = &(mbr.mbr_partition[0]);
              Partition * tmp1 = &(mbr.mbr_partition[1]);
              Partition * tmp2 = &(mbr.mbr_partition[2]);

              free_space = sizeof(Mbr) - tmp->part_start;
              //printf("espacio libre mbr-primeraParticion: %d\n", free_space);
              if(p_size <= free_space){
                p_start = sizeof(Mbr);
              }else{
                free_space = getEspacioLibre(tmp->part_start, tmp->part_size, tmp1->part_start);
                //printf("espacio libre primeraParticion-segundaParticion: %d\n", free_space);
                if(p_size <= free_space){
                  p_start = (tmp->part_start + tmp->part_size);
                }else{
                  free_space = getEspacioLibre(tmp1->part_start, tmp1->part_size, tmp2->part_start);
                  //printf("espacio libre segundaParticion-terceraParticion: %d\n", free_space);
                  if(p_size <= free_space){
                    p_start = (tmp1->part_start + tmp1->part_size);
                  }else{
                    free_space = mbr.mbr_tamanio - (tmp2->part_start + tmp2->part_size);
                    //printf("espacio libre terceraParticion-finDisco: %d\n", free_space);
                    if(p_size <= free_space){
                      p_start = (tmp2->part_start + tmp2->part_size);
                    }else{
                      procede == FALSE;
                    }
                  }
                }
              }


              if(procede == TRUE){
                printf("%s%d\n", "espacio libre: ",free_space);
                printf("\t%s %s\n", "Existe epacio suficiente en disco, se Procedera a crear la particion: ", data->name);
                configPartition(&(mbr.mbr_partition[3]), PARTITION_STATUS_ACTIVE,
                                                         data->type,
                                                         data->fit,
                                                         p_start,
                                                         p_size,
                                                         data->name);
              }else{
                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
              }
            }
          }
          if(procede == TRUE){
            sortPartitions(&mbr);
            FILE * f = fopen(data->path, "r+b");
            rewind(f);
            fwrite(&mbr, sizeof(Mbr), 1, f);
            fclose(f);
            alert(SCS, "Particion creada Exitosamente.");
          }
        }
      }

    }else{
      if(data->size == -1 && strlen(data->del) != 0)
      {
        if(data->add == 0){
          if(data->type == ' '){
            if(strlen(data->fit) == 0){
              if(data->unit == ' '){
                Mbr mbr;
                if (getMbr(&mbr, data->path))
                {
                  Partition *array[4];
                  for(int i = 0; i<4; i++){
                    array[i] = &(mbr.mbr_partition[i]);
                  }
                  Partition *(*p)[] = &array;
                  printf(CYN "################### ACCESANDO AL DISCO ###################\n" RESET);
                  printf("date of disk creation: %s\n",mbr.mbr_fecha_creacion);
                  printf("amount of bytes: %d \n", mbr.mbr_tamanio);
                  printf("amount of mbr's bytes: %d \n", sizeof(Mbr));

                  bool foud_it = FALSE;
                  for (int i = 0; i < 4; i++)
                  {
                      Partition * partition = (*p)[i];
                      if(partition->part_status == PARTITION_STATUS_ACTIVE){
                        if(strcmp(data->name, partition->part_name) == 0){
                          if(stricmp(data->del, "fast") == 0){
                            configEmptyPartitionFast(&(mbr.mbr_partition[i]));
                          }else if(stricmp(data->del, "full") == 0){
                            configEmptyPartition(&(mbr.mbr_partition[i]));
                          }
                          i=4;
                          foud_it = TRUE;
                        }
                      }
                  }
                  if(foud_it == FALSE){
                    alert(ERR, "Particion NO ENCONTRADA en el disco.");
                  }else{
                    sortPartitions(&mbr);
                    FILE * f = fopen(data->path, "r+b");
                    rewind(f);
                    fwrite(&mbr, sizeof(Mbr), 1, f);
                    fclose(f);
                    printf("\tParticion: %s encontrada exitosamente, se procedera a eliminarla.\n", data->name);
                    alert(SCS, "Particion eliminada Exitosamente.");
                  }
              }
              }else{
                alert(WAR, "parametro -unit debe ser NULO");
              }
            }else{
              alert(WAR, "parametro -fit debe ser NULO");
            }
          }else{
            alert(WAR, "parametro -type debe ser NULO");
          }
        }else{
          alert(WAR, "parametro -add debe ser NULO");
        }
      }
    }
  }
  free(data);
}

int getEspacioLibre(int ps1, int psz1, int ps2){
  int inicio_libre = ps1 + psz1;
  int fin_libre = ps2;
  return fin_libre - inicio_libre;
}

void newDisk(mkDisk * data){

  if(data->valid == TRUE){


    FILE * f = fopen(data->path, "wb");
    if(f != NULL){
      int bytes;
      if(data->unit == 'k'){
          bytes = 1024*data->size;
      }else if(data->unit == 'm'){
          bytes = 1024*1024*data->size;
      }

      unsigned char byte[bytes];

      for (int i = 0; i < bytes; i++)
      {
          byte[i] = 0;
      }

      fwrite(byte,sizeof(byte),1,f);

      rewind(f);

      Mbr mbr;
      mbr.mbr_tamanio = bytes;
      getDate(mbr.mbr_fecha_creacion);

      mbr.mbr_disk_signature = rand();

      for(int i = 0; i<4; i++){
        configEmptyPartition(&(mbr.mbr_partition[i]));
        /*configPartition(&(mbr.mbr_partition[i]), PARTITION_STATUS_ACTIVE,
                                                 'p',
                                                 "bf",
                                                  i,
                                                 (i+1)*20,
                                                 "part");*/
      }

      sortPartitions(&mbr);

      fwrite(&mbr, sizeof(Mbr), 1, f);
      fclose(f);
      alert(SCS, "Disco creado Exitosamente.");
    }
  }
  free(data);
}

void delDisk(rmDisk * data){
  if(data->valid == TRUE){
    remove(data->path);
    alert(SCS, "Disco eliminado Exitosamente.");
  }
  free(data);
}

void genRep(Rep * data){
  if(data->valid == TRUE){
    //data->name
    //data->path
    //data->id
    Mbr mbr;
    Mbr * pointer;
    if (getMbr(&mbr, data->path))
    {
      pointer = &mbr;
      if(stricmp(data->name, "mbr") == 0){
        graphMbr(pointer, "/home/fernando/Desktop/reps/mbr_rep.gv");
        alert(SCS, "Reporte \"mbr\" creado Exitosamente.");
      }else if(stricmp(data->name, "disk") == 0){
        graphDisk(pointer, "/home/fernando/Desktop/reps/disk_rep.gv");
        alert(SCS, "Reporte \"disk\" creado Exitosamente.");
      }
    }
  }
}

void mountPartition(mount * data){
  if(data->valid == TRUE){
    Mbr mbr;
    if (getMbr(&mbr, data->path))
    {
      Partition *array[4];
      for(int i = 0; i<4; i++){
        array[i] = &(mbr.mbr_partition[i]);
      }
      Partition *(*p)[] = &array;
      bool found = FALSE;
      for (int i = 0; i < 4; i++)
      {
          Partition * partition = (*p)[i];

          if (strcmp(partition->part_name, data->name)==0){
            printf("%s\n", partition->part_name);
            insert_front(mounts, "vda1", data->path, data->name);
            alert(SCS, "Particion montada Exitosamente.");
            printf(CYN "############## PARTICIONES MONTADAS ACTUALMENTE ##############\n" RESET);
            print(mounts);
            printf("\n\n");
            found = TRUE;
          }
      }
      if(!found){
        alert(WAR, "Particion NO ENCONTRADA, no se puede montar la particion especificada.");
        printf("%s\n", "");
      }
    }

  }
}

void unMountPartition(unMount * data){
  if(data->valid == TRUE){
    remove_any(mounts, data->id);
    if(mounts->head != NULL){
      printf(CYN "############## PARTICIONES MONTADAS ACTUALMENTE ##############\n" RESET);
      print(mounts);
    }
    printf("\n\n");
  }
}
